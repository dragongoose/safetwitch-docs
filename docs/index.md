# API Documentation
### Disclaimer

Every endpoint can return a 500 status code, and it follows this schema:

```json
{
  status: "error",
  data: "Error message..."
}
```

### Continue to documentation
API documentation is [here](/API/api)