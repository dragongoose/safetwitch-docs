## Proxying Endpoints

### /proxy/img/base64Url

**GET** - Proxies an image through the server  
:base64Url can be any base64 encoded Url

#### Responses
**200**  
Server returns the requested image

**404**  
The requested image was invalid


### /proxy/stream/xqc/hls.m3u8

**GET**
Gets the m3u8 manifest for a streamer. This manifest will contain all stream qualities if they are live

#### Responses
**200**  
Returns the manifest

**400**  
The streamer is not live  
*Example:*

```json
{
  "status": "error",
  "data": "Streamer is not live"
}
```

### /proxy/stream/sub/encodedUrl

**GET** - Returns the m3u8 manifest for a specific quality under the [master manifest](#/proxy/stream/:username/hls.m3u8)  

Note, you should never need to touch this, it's handled by the player parsing the manifest file.
#### Responses
**200**  
Returns the manifest file

### /proxy/stream/segment/encodedUrl

**GET** - Gets a segment from one of the quality's manifest file. This is the actual video thats displayed on your screen

Note, you should never need to touch this, it's handled by the player parsing the manifest file.

**200**  
Returns the stream segment, HLS streaming.

### /proxy/vod/:vodID/video.m3u8
**GET** - Gets the master manifest for a VOD  

Set this to the source of your video player to watch the stream!

#### Responses
**200**  
Returns### the manifest file

### /proxy/vod/sub/:encodedUrl/video.m3u8
**GET** - Gets the sub manifest for a VOD    
encodedUrl is the url to the sub manifiest from twitch encoded in base64  

Note, you should never need to touch this, it's handled by the player parsing the manifest file.

#### Responses
**200**  
Returns the manifest file

### /proxy/vod/sub/:encodedUrl/:segment
**GET** - Gets the sub manifest for a VOD    
encodedUrl is the url to the sub manifiest from twitch encoded in base64  

segment is the segment from the playlist file, etc 0.ts, 1.ts

Note, you should never need to touch this, it's handled by the player parsing the manifest file.

#### Responses
**200**  
Returns the manifest file

